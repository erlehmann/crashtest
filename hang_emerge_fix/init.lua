local minetest_emerge_area = minetest.emerge_area
minetest.emerge_area = function(minp, maxp, ...)
	if
		minp.x >= 32767 or minp.x <= -32768 or
		minp.y >= 32767 or minp.y <= -32768 or
		minp.z >= 32767 or minp.z <= -32768 or
		maxp.x >= 32767 or maxp.x <= -32768 or
		maxp.y >= 32767 or maxp.y <= -32768 or
		maxp.z >= 32767 or maxp.z <= -32768
	then
		minetest.log(
			"warning",
			"minetest.emerge_area() called with coords outside interval (-32768, 32767), skipping: " ..
			"minp { x=" .. minp.x .. ", y=" .. minp.y .. " z=" .. minp.z .. " } " ..
			"maxp { x=" .. maxp.x .. ", y=" .. maxp.y .. " z=" .. maxp.z .. " } "
		)
	else
		return minetest.emerge_area(minp, maxp, ...)
	end
end
